/*
 * Copyright (c) 2019 David Baberowski
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
 * associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <Servo.h>
# include <Stepper.h>

Stepper StepperX(2048,9,11,10,12);
Stepper StepperY(2048,2,4,3,5);
Servo ServoZ;

//use your own Calibration
float stepsPerMillX = 100.0/9.0;
float stepsPerMillY = -500.0/9.0;

float currentX = 0;
float currentY = 0;

void setup() {
  ServoZ.attach(A0);
  StepperX.setSpeed(6);
  StepperY.setSpeed(6);
}

void loop() {
  // put your G-Code here
  penDown();
  delay(500);
  G1(10,10);
  
  penUp();
  delay(500);
  G1(0,0);
}

//Use this method as the G1 Command
void G1(float x, float y){
  int stepsX = (x - currentX)*stepsPerMillX;
  int stepsY = (y - currentY)*stepsPerMillY;

  int doneX = 0;
  int doneY = 0;

  for(int i=0; i<1000;i++){
    if(abs(doneX-stepsX)
  }

  currentX = x;
  currentY = y;
}

void penUp(){
  ServoZ.write(20);
}

void penDown(){
  ServoZ.write(80);
}
